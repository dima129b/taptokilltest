﻿using UnityEngine;
using UnityEngine.EventSystems;


public class PointsManagement : MonoBehaviour, IPointerClickHandler
{
    public static float score;

    public void OnPointerClick(PointerEventData eventData)
    {
        score += gameObject.GetComponent<Objects>().property;
        gameObject.SetActive(false);
    }
}

