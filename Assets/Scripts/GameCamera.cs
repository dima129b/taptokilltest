﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private static Vector2 border = new Vector2(0f, 0f);

    // растояние до границы экрана
    public static Vector2 Border
    {
        get
        {
            if (border.x == 0 && border.y == 0)
            {
                var cam = Camera.main;
                border.x = cam.aspect * cam.orthographicSize;
                border.y = Camera.main.orthographicSize;
            }
            return border;
        }
        private set { }
    }
}
