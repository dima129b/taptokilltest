﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] private float spawnTime; // время между спауном врагов

    void Start()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(6f);
        if (spawnTime == 0)
        {
            Debug.LogError("Не выставлено время спауна");
            spawnTime = 0.5f;
        }
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);

            float xPos = Random.Range(-GameCamera.Border.x + 1f, GameCamera.Border.x - 1f);
            float yPos = Random.Range(-GameCamera.Border.y + 0.7f , GameCamera.Border.y - 0.7f);

            GameObject objectPrefab = ObjectPooler.SharedInstanse.GetPooledObject("objectPrefab");
            if (objectPrefab != null)
            {
                objectPrefab.transform.position = new Vector3(xPos, yPos, 90f);
                objectPrefab.transform.rotation = Quaternion.identity;
                objectPrefab.SetActive(true);
            }
        }
    }
}

