﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objects : MonoBehaviour
{
    public float property;
    [SerializeField] private List<ObjectsData> objectsSettings;
    private SpriteRenderer spriteRenderer;

    private float time = 0;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        time += Time.fixedDeltaTime;

        if (time >= 3f)
        {
            gameObject.SetActive(false);
        }
    }
    private void OnEnable()
    {

        time = 0;
        // генерация случайного
        int rand = Random.Range(0, objectsSettings.Count);
        ObjectsData randObjectSeting = objectsSettings[rand];

        spriteRenderer.sprite = randObjectSeting.Sprite;
        spriteRenderer.color = randObjectSeting.Color;
        property = randObjectSeting.Property;
    }   

}
