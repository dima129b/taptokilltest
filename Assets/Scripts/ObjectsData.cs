﻿using UnityEngine;


[CreateAssetMenu(menuName = "Objects/Create object", fileName = "New Object")]
public class ObjectsData : ScriptableObject
{

    [SerializeField] private Sprite sprite;
    public Sprite Sprite
    {
        get { return sprite; }
        protected set { }
    }

    [SerializeField] private float property;
    public float Property
    {
        get { return property; }
        protected set { }
    }

    [SerializeField] private Color color;
    public Color Color
    {
        get { return color; }
        protected set { }
    }
}
