﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Buttons")]
    [SerializeField] private Button PauseButton;
    [SerializeField] private Button BackToGameButton;
    [SerializeField] private Button RestartButton;
    [SerializeField] private Button ExitButton;

    [Header("Panels")]
    [SerializeField] private GameObject panelPause;
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private GameObject panelStartTip;

    [Header("Text")]
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private Text scoreTextWin;

    [Header("Variables")]
    [SerializeField] private float time = 60f;


    void Start()
    {
        panelStartTip.SetActive(true);

        StartCoroutine(Timer());
    }

    private void Update()
    {
        scoreText.text = PointsManagement.score.ToString();
    }

    public void Pause()
    {
        panelPause.SetActive(true);
        Time.timeScale = 0;
    }

    public void BackToGame()
    {
        Time.timeScale = 1;
        panelPause.SetActive(false);
    }

    public void ReStart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
        PointsManagement.score = 0;
        time = 60f;
        scoreText.text = PointsManagement.score.ToString();
    }

    public void GameOver()
    {
        panelGameOver.SetActive(true);
        scoreTextWin.text = PointsManagement.score.ToString();
        Time.timeScale = 0;
    }

    public void Exit()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
        PointsManagement.score = 0;
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(6f);
        panelStartTip.SetActive(false);

        while (time >= 0.0f)
        {
            time -= Time.deltaTime / 2;
            float time2 = Mathf.FloorToInt(time);
            timeText.text = time2.ToString();
            yield return new WaitForFixedUpdate();
        }

        GameOver();

        yield return null;
    }
}


